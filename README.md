# f-numbers.json

`f-numbers.json` contains an array of conventional f-numbers for full-stops, half-stops, and third-stops. Each array ranges from f/0.7 to f/90.

Note that these numbers are simply the ones most cameras use. This should not be confused with mathematically calculated f-numbers.
